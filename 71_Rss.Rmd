---
title: "181 Rss"
author: "ArtMG"
date: "2025-12-06"
knit: (function(input, ...) {rmarkdown::render(input, output_dir = ".knitted")})
---

# Introduction

An RSS document, or 'feed' is stored at a URL to give a 'Rich Site Summary', 
and is commonly used as an index for serial publications such as blogs, 
podcasts, newsfeeds, package releases, etc.

## package choices

At the end of the day, RSS documents are structured HMTL pages, and the 
regular packages like `xml2`, and probably `httr2`, will allow you to do 
whatever you might need with them. There are a couple of more specific 
packages that you might find make you task easier:

The most obvious candidate package is **`tidyRSS`**, which produces tibbles 
to easily handle simpler cases. Related packages include tidygeoRSS for geo- 
related functions, and newscatchr which wraps tidyRSS.
`feedr` returned nested lists so you could 
do more exacting work even with complex data, but it is no longer in CRAN, 
and `r-does-rss` simply stalled in development. 


## Principles

Useful things to understand about this project and using it

* 
    * 
* 


## Prereqs

Before using this you will need to have (done) the following:

* ` install.packages(c("tidyRSS")) `
  - if you have tidyverse installed that statisfies most dependencies
  - it also uses `anytime` flexible date converter and `BH` (Boost C++ headers)


## doc features
Here is a classic dogfood test of using tidyfeed to get the releases of 
its own package...

```{r dog food sample}
# you will need to have pasted in your environment cod block before running this
tidyRSS::tidyfeed("https://github.com/RobertMyles/tidyRSS/releases.atom")
```

```{r stop knit here}
knitr::knit_exit()
```




# START HERE  ################################

We recommend you run all sections before this, then you can start
* paste in your environment secrets chunk
* click the Run All Chunks Above button 
* check the smoke test
* begin pasting sections from below into the console
    * or use shortcut like Cmd-Enter to execute line by line


## Use

```{r use extra functions}

```

```{r use examples}

# tidyRSS::tidyfeed(

```

### Use with other notebook

This sets up the pre-requisites for running the code chunk below, 
and is like going into both these and running the functions up to Start Here

```{r set up prereqs}
# use the knit process (called by render) to run the pre-req code
rmarkdown::render("23_other_notebook.Rmd", output_dir = ".knitted", quiet = TRUE)
rmarkdown::render("00_self_if_needed.Rmd", output_dir = ".knitted", quiet = TRUE)
```

```{r run code with functions from elsewhere}

```


## Test

QA the code in specific circumstances

```{r test basics}

```

```{r test specific, eval=FALSE}

```

## Develop

```{r dev basics}

```

```{r dev specific}

```

