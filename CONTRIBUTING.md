This is intended as a guide for developing this project further, whether for your own ends, or to contribute back into the upstream. For more details about the project itself see [README.md](README.md)

## Development approach

This project is intended to be used interactively, and perhaps improved iteratively as you prototype new data interactions. The [R Markdown](https://r4ds.had.co.nz/r-markdown.html) format for combining documentation, instructions, code and results is ideal for working in this way. The general workflow for prototyping and developing the code here is the [R Markdown workflow](https://r4ds.had.co.nz/r-markdown-workflow.html)

The project is starting off with a flat structure, but as it grows it could fall into more of a typical R project structure, separating docs and perhaps some src (source code) into distinct folders. See a couple of guides on structure and naming practices typical with R projects: [1 r-bloggers](https://www.r-bloggers.com/2018/08/structuring-r-projects/) and [2 kdestasio](https://kdestasio.github.io/post/r_best_practices/)

The data flows are less traditionally in and out with this being about data exchange and migration, so instead of being a subfolder of this repo, the code chunks that contain secrets also typically state which local folder you want to use to hold any temporary files used to inspect or exchange data. 

## Useful R packages

This project relies on R Packages for the libraries that do a lot of the 'heavy lifting'. The [CRAN Task View: Web Technologies and Services](https://cran.r-project.org/web/views/WebTechnologies.html) might introduce you to others that you can use for whatever data services you want to interface with. 

The `tidyverse` brings together a lot of useful libraries into a single, simple to install entity. Generally we have a liking for techniques that use libraries in the **tidyverse** – see the [tidyverse cookbook](https://rstudio-education.github.io/tidyverse-cookbook/). Having said that, if it is simple to execute a task using '**base R**' syntax, you might prefer to use that. 

Although it is not yet included in the tidyverse, and some packages still use httr, that package classes itself as deprecated, and there are lots of valuable features for API interaction in the [httr2]() package, so that is our mainstay for the calls under the covers. 

You should read the vignettes about [Wrapping APIs in httr2](https://httr2.r-lib.org/articles/wrapping-apis.html) if you are going to spend much time working on the API calling functions. Perhaps the best practices could be pulled into a common set of functions, used by many individual services. 


## Other useful knowledge

* [OpenAPI](https://www.openapis.org/) is a standard for documenting APIs that some service vendors may make available.
	* Swagger is an early implementation of this standard for generating code
	* the [rapiclient](https://cran.r-project.org/web/packages/rapiclient/index.html) package is a Dynamic OpenAPI/Swagger Client
		* meant to access services specified in OpenAPI with a client that is generated dynamically as a list of R functions. it does not generate code. 
		* see also 
	* the third party [OpenAPI Generator for the R language](https://openapi-generator.tech/docs/generators/r/) does generate code for you
	* Contribute elsewhere! Add relevant items into https://openapi.tools/#converters with a PR
* This [ROpenSci blog article from summer 2022](https://ropensci.org/blog/2022/06/16/publicize-api-client-yes-no/) discusses some of the ins and outs of building an API client, including the use of OpenAPI where available
* if you are considering Shiny apps, see [issue about oAuth in Shiny](https://github.com/r-lib/httr2/issues/47)


## Interpreting structured data

The payloads returned by APIs ... (_continue explanation_)

* [Rectangling in R for Data Science](https://r4ds.hadley.nz/rectangling)
* [tidyverse vignette on rectangling](https://tidyr.tidyverse.org/articles/rectangle.html#game-of-thrones-characters)
* 

If there are techniques that we start using very commonly, we might consider encapsulating these as common functions. However it is unlikely that they will give such great additional value that we would consider [making them into a package](https://r-pkgs.org/).

## Less structured data

You might come across a data source that is not exposed as an API, in which case you might have to resort to web scraping. `rvest` is in the tidyverse...

* [Web scraping in R for Data Science](https://r4ds.hadley.nz/webscraping)
* [using `rvest` for Web Scraping](https://statsandr.com/blog/web-scraping-in-r/#web-scraping-in-r)
* [the rvest package documentation](https://cran.r-project.org/web/packages/rvest/rvest.pdf) and [vignettes](https://cran.r-project.org/web/packages/rvest/vignettes/)
* [scraping bee's introduction to scraping in R](https://www.scrapingbee.com/blog/web-scraping-r/)
* 


## Looking further ahead

One day it might be nice to help all sorts of people unlock access to their data. 
Cloud software vendors say that they respect your data and always allow you to access it. 
However, that often means an XML dump, and to most people this is about as much use as if 
you bought a kit car in a thousand pieces as a gift for a child who just passed their driving test. 
Or alternatively the service offers an API, which most people would find even harder - 
hence this vision.

It is likely that a more widely used language like Python might lower the barriers to 
people picking it this up and using it. It is supposed to be the most widely known language outside of professional software engineering circles, and maybe an IDE like Spyder might offer the kind of exploratory data analysis features that R coders have grown to love in RStudio.

Ideally though, in the longer run, it would be great if there were some way to describe 
the input and output data via a model that does not require the end-user to program. 
I'm not saying it has to be wysiwyg, 
but at least some low-code way of linking up the jigsaw pieces from one system into another.

