---
title: "10_Trello"
author: "ArtMG"
date: "2024-09-18"
knit: (function(input, ...) {rmarkdown::render(input, output_dir = ".knitted")})
---

* if you have previously set up this app, 
  * and have you secret code block saved somewhere
  * you need to **REFRESH THE TOKEN** in Environment
  * then skip down to # START HERE ##########

# Introduction


## Trello

The docs for API reference 
https://developer.atlassian.com/cloud/trello/rest/
include language snippets in 

* curl
* Node.js
* Java
* Python
* PHP

### Authentication

To get and use your API key...

* paste the following code chunk somewhere secure
  * bearing in mind the token will only last a day
  * but the rest can be reused

```r
Sys.setenv("APP_BASEURL"="https://api.trello.com/1")
Sys.setenv("APP_APIKEY"="a1b2c3d4Z0Y9X8W7")
Sys.setenv("APP_TOKEN"="a1b2c3d4Z0Y9X8W7a1b2c3d4Z0Y9X8W7")
Sys.setenv("APP_FILEPATH"="~/Cached/R/")
```

#### Alternative browser for reauthorisation

* If you WISH to modify which browser and profile the auth dialog opens in:
  * For Google Chrome
    * open the browser and profile you want to use
    * browse to ` chrome://version/ `
    * check the Profile Path
    * use the Profile Path in the profile-directory option in the following
* Note that these env options are macos specific and may need tweaking for other OSes
* you may wish to change the APP_AUTH_SCOPE to "read,write"

```r
# optional environment settings for an alternative browser for reauthorisation
Sys.setenv("R_BROWSER"='open -n -a "Google Chrome" --args --profile-directory="Profile 2"')
Sys.setenv("APP_AUTH_SCOPE"="read")
options(browser = Sys.getenv("R_BROWSER"))
```

* now, in a browser where your account is signed in, visit

https://trello.com/power-ups/admin

* Create an new PowerUp, called something like MyApiAuth
* Generate an API key
* Copy the API key and paste into the code chunk from above



# Environment

## Reauthorise

You will have you code chunk saved securely, but your token needs refreshing daily

* use your Api Key from that chunk...

```r
Sys.setenv("APP_APIKEY"="a1b2c3d4Z0Y9X8W7")
```

* ...together with the relevant scope from below, to get a url

```r
scope <- "read,write"
scope <- "read"
paste0("https://trello.com/1/authorize?expiration=1day&name=MyPersonalToken&scope=",scope,"&response_type=token&key=",Sys.getenv("APP_APIKEY"))
```

* Paste the url into the correct browser and profile to authenticate
* once you have given authorisation, 
  * copy your token from the auth page 
  * and paste into the secure chunk to replace the previous token

## Use refreshed environment chunk

Now you can Paste in the whole code chunk to authorise api calls


For more see 

* https://developer.atlassian.com/cloud/trello/guides/rest-api/authorization/

### Trello API basics

According to https://developer.atlassian.com/cloud/trello/guides/rest-api/api-introduction/#boards
the nouns you put in the request route may be plural or singular
 - /card and /cards work the same

When getting items such as boards or cards you may refer to them
using either the full 24 alpha id or the shortlink slug from the url


```{r app functions}
library(httr2)

### create generic app request

app_req <- function(route, verb) {
  # create the request for your app API call request
  # build the API url from the root plus the route specified
  # then add in the auth header using auth method 2 from
  # https://developer.atlassian.com/cloud/trello/guides/rest-api/authorization/#passing-token-and-key-in-api-requests
  req <- request(Sys.getenv("APP_BASEURL")) %>%
  	req_url_path_append(route) %>% 
    req_headers(Authorization = paste0(
      'OAuth oauth_consumer_key="',Sys.getenv("APP_APIKEY")
      ,'", oauth_token="',Sys.getenv("APP_TOKEN")
      ,'"')) 
  
  # if a verb is specified, then use it
  if (!missing(verb)) req <- req_method(req,verb)

  return(req)  
}

app_full_resp <- function(req) {
# try the API and either return the response with message OK
  error_message <- tryCatch({
    req_perform(req)
    "OK"
  }, error = function(e) {
      e$message
    }
  )

  resp <- last_response()
  status_code <- resp$status_code

  message <- switch(as.character(status_code),
    "200" = "OK"
    ,"400" = paste("Request Failed: ", resp_body_string(resp))
    ,"401" = paste("Not authorised: ", resp_body_string(resp))
    ,"500" = "invalid API call"
    ,paste("Unknown status",resp$status_code,"with message:",error_message)
  )

  response_data <- NA
  if (status_code == 200) response_data <- resp_body_json(resp)

  list(
      response = resp
      , status = status_code
      , message = message
      , response_data = response_data)
}

app_get_data <- function(route) {
  app_req(route) |> app_full_resp() |> getElement("response_data")
}



app_full_resp_2 <- function(req) {
# try building an alterantive wrapper that uses req_error instead
# https://httr2.r-lib.org/reference/req_error.html
  tryCatch({
    resp = req_perform(req)
    status_code <- resp$status_code

    message <- switch(as.character(status_code),
      "200" = "OK"
      ,"400" = paste("Request Failed: ", resp_body_string(resp))
      ,"500" = "invalid API call"
      ,paste("Unknown status",resp$status_code)
    )

    response_data <- resp_body_json(resp)

    list(
        response = resp
        , status = status_code
        , message = message
        , response_data = response_data)
    }, error = function(e) {
      # or get the response anyhow and include the message
      list(response = last_response(), status = status_code, message = e$message)
    }
  )
}


df_from_full_response <- function(full_resp) {
  # return a dataframe based on the main list of lists 
  # in an app API full response
  resp_list <- full_resp$response_data
  flat_list <- purrr::map(resp_list, purrr::flatten)
  suppressWarnings(
    data.table::rbindlist(flat_list, use.names = TRUE, fill = TRUE)
  )
}

list_as_df <- function(nested_list) {
  # return a dataframe based on the supplied nested list
  nested_list |> 
    lapply(lapply, function(x) {
      if (is.list(x) && length(names(x))==0) {
        # un-named lists are converted to comma delim strings
        paste(unlist(x),collapse = ", ")
        # and everything else stays as is for now
      } else x }) |>
    # list_flatten kindly expands those named lists (e.g. customer fields)
    purrr::map(purrr::list_flatten) |>
    dplyr::bind_rows()
}


last_error_response <- function() resp_body_string(last_response())

trello_reauthorise <- function(scope=NULL) {
  # else if you need, call with   scope="read,write"
  if (is.null(scope)) {scope <- Sys.getenv("APP_AUTH_SCOPE") }
  
  re_auth_url <- paste0("https://trello.com/1/authorize?expiration=1day&name=MyPersonalToken&scope=",scope,"&response_type=token&key=",Sys.getenv("APP_APIKEY"))

  browseURL(re_auth_url, browser = getOption("browser"))
}
```

``` {r trello cards and lists}

get_board <- function(board_id) {
  app_get_data(paste0("/boards/",board_id))
}

get_lists <- function(board_id) {
  app_get_data(paste0("/boards/",board_id,"/lists"))
}

get_cards <- function(board_id) {
  app_get_data(paste0("/boards/",board_id,"/cards"))
}

get_card <- function(card_id) {
  app_get_data(paste0("/cards/",card_id))
}

get_card_attachments <- function(card_id) {
  app_get_data(paste0("/cards/",card_id,"/attachments"))
}

get_checklist <- function(checklist_id) {
  app_get_data(paste0("/checklists/",checklist_id))
}

create_card <- function(item_data,parent_id) {
  req <- app_req(paste0("/cards?idList=",parent_id)) %>%
    req_body_json(item_data) # req_body infers POST
  fresp <- app_full_resp(req)
}

get_checklists_for_cards <- function(cardsList) {
  lapply(cardsList, function(card) {
    lapply(card$idChecklists, get_checklist)
  })
}

get_checkitems_from_card <- function(card_id) {
  # get card and handle exceptions
  card <- get_card(card_id)
  if (length(card)==1) if (is.na(card)) return(NA) # or list() ?
  if (length(card$idChecklists) == 0) return(list())
  # get list containing item names
  checklist <- get_checklist(card$idChecklists[[1]])
  # for migration we will need to also get state 
  # and due might be needed somewhere 
  return(lapply(checklist$checkItems,'[',c("name","state", "due")))
}

```

``` {r migration functions}

create_list_update_board <- function(new_idBoard,list_item) {
  # save old pointers
  list_item$idOld <- list_item$id
  list_item$idOldBoard <- list_item$idBoard

  # instruct the item where to create
  list_item$idBoard <- new_idBoard
  list_item$id <- NULL # leave case of failure
  # only send required data
  item_data_mask <- c("name","closed","color","idBoard","pos","subscribed","softLimit")
  item_data <- list_item[item_data_mask]

  # create_list could become function?
  # shouldn't need query param if idBoard in data, and req_body infers POST
  f_resp <- app_req(paste0("/lists")) %>%
    req_body_json(item_data) %>% app_full_resp()

  list_item$id <- f_resp$response_data$id
  # return updated item
  list_item
}

make_dict <- function(dest_items) {
  # make a 'dictionary' named vector as a lookup table,
  # by putting in the new ids and adding the names as the old ids
  item_dict <- unlist(purrr::map(dest_items, purrr::pluck, "id"))
  names(item_dict) <- unlist(purrr::map(dest_items, purrr::pluck, "idOld"))
  item_dict
}

create_card_update_list <- function(list_dict,card_item) {
  # save old pointers
  card_item$idOld <- card_item$id
  card_item$idOldBoard <- card_item$idBoard
  card_item$idOldList <- card_item$idList

  # look up in the list dictionary where to create new card
  card_item$idList <- as.character(list_dict[card_item$idList])
  card_item$id <- NULL # leave case of failure

  # only send required data
  item_data_mask <- c("idList", "name", "desc", "due", "start", "dueComplete")
  #item_data_mask <- c("idList", "name", "desc", "pos", "due", "start", "dueComplete")
  item_data <- card_item[item_data_mask]

  # create_list could become function?
  # shouldn't need query param if idBoard in data, and req_body infers POST
  f_resp <- app_req(paste0("/cards")) %>%
    req_body_json(item_data) %>% app_full_resp()

  card_item$id <- f_resp$response_data$id
  card_item$idBoard <- f_resp$response_data$idBoard
  # return updated item
  card_item
}


```

## doc features
```{r set up smoke test}
trello_smoke_test <- function() {
  api_test <- app_req("/members/me") |> app_full_resp()
  if (api_test$status == 401) {
    warning("Please reauthorise in your browser")
    trello_reauthorise()
    stop("After reauthorising you must use the new APP_TOKEN into your Sys.setenv() block")
  }
  if (api_test$status == 200) {
    me <- getElement(api_test,"response_data")
    paste("Hello",me$fullName,"- user:",me$username)
  } else {
    api_test$message
  }
}
```

```{r stop knit here}
knitr::knit_exit()
```

```{r run smoke test}
# you will need to have pasted in your environment cod block before running this
trello_smoke_test()

```


# START HERE  ################################

We recommend you run all sections before this, then you can start
* paste in your environment secrets chunk
* click the Run All Chunks Above button 
* check the smoke test
  * if your token is invalid it might need Re-authorising
    * see section 'Environment' above
* begin pasting sections from below into the console
  * or use shortcut like Cmd-Enter to execute line by line


## Use

### migration approach

* Source board_id
* Get full old_board_id
* Get Lists (Trello Lists not r lists)
* Dest board_id
* Get full new_board_id
* for each List
* add new List and store new List id to look up against old
* use stored List ids to create lookup dictionary

* Get cards
* for each card get sub items (Actions, ?)

* for each card
* look up new list id against old
* Create on new list
* Store new list_id and new card_id


``` {r migration approach}

# board_id from below

src_lists <- get_lists(board_id)
# was not used
## these 'board_id's are just the shortlink slugs from the url
## let's get the full 24 hex TrelloID
#idBoard <- src_lists[[1]]$id
src_cards <- get_cards(board_id)

### SWITCH CONTEXT

# new_board_id <- from new context
new_idBoard <- get_board(new_board_id)$id

#code TBC # set the new board attributes to match the source

dest_lists <- lapply(src_lists, 
  function(i) create_list_update_board(new_idBoard,i))

list_dict <- make_dict(dest_lists)

dest_cards <- lapply(src_cards, 
  function(i) create_card_update_list(list_dict,i))

card_dict <- make_dict(dest_cards)

```


## Tests

```{r test misc}

# some tests

list_item <- src_lists[[2]]
create_list_update_board(new_idBoard,list_item)

card_item <- src_cards[[1]]
new_card_item <- create_card_update_list(list_dict,card_item)
```



## Dev

``` {r dev explore cards and lists}


cards <- get_cards(board_id)

last_error_response()

lastcard <- cards[[1]]

```

``` {r misc exploration}


lists <- app_req(paste0("/boards/",board_id,"/lists")) %>%
  app_full_resp() %>% df_from_full_response()

req <- app_req(paste0("/boards/",board_id,"/lists"))

fresp <- app_full_resp(req)
lists <- df_from_full_response(fresp)

fresp <- app_full_resp(app_req(paste0("/boards/",board_id,"/cards")))

actions <- app_req(paste0("/cards/",card_id,"/actions")) %>%
  app_full_resp() %>% getElement("response_data")



card <- list(
  idList = list_id
  , name = "my card"
  , desc = "sure is"
)
  req <- app_req("/cards") %>%
    req_body_json(card)
  fresp <- app_full_resp(req)

card <- list(
  name = "other card"
  , desc = "yessireebob"
)
  req <- app_req(paste0("/cards?idList=",list_id)) %>%
    req_body_json(card)
  fresp <- app_full_resp(req)

card <- cards[[1]]
names_to_keep <- c("name", "desc", "pos", "due", "start", "dueComplete")
names_to_keep <- c("name", "desc", "due", "start", "dueComplete")
test_card <- card[names_to_keep]
resp <- create_card(card,list_id)

dict_test <- c("f1"="name", "f2"="desc", "f3"="pos", "f4"="due", "f5"="start", "f6"="dueComplete")

```

```{r dev checklists}
# subset to filter cards by partial match on name
matching_cards <- cards[grep("ate",lapply(cards,'[[',"name"))]
card <- matching_cards[[1]]
checklist_id <- card$idChecklists[[1]]

some_cards <- cards[24:28]
some_checklists <- get_checklists_for_cards(some_cards)

card_id <- 'NmStSTol'
card <- get_card(card_id)
checklist_id <- card$idChecklists[[1]]
checklist <- get_checklist(checklist_id)
checklist_items <- lapply(checklist$checkItems,'[',c("name","state"))

items <- get_checkitems_from_card(card_id)

```

```{r dev attachments}
# cards have an indicator that there is an attachment in $badges$attachments
cards_with_attachments <- Filter(function(x) x$badges$attachments > 0, src_cards)

# the card_id does not explicitly come in the attachment data, but it 
# could be extrapolated from the url
get_card_attachments_with_ids <- function(card_id){
  # inject the card_id into each attachment sublist
}
cards_attachments <- lapply(cards_with_attachments , 
       function(x) get_card_attachments(x$id))
# then we should unlist the top level so they are all one list of lists

file_names <- unlist(lapply(purrr::list_flatten(cards_attachments), '[[', "fileName"))

car <- get_card("0h4WIEXA")
att <- get_card_attachments("0h4WIEXA")


```


