# R API Xchanger (r_api_x)

Use the R language and libraries to import, export and migrate data between different cloud tools' APIs. 

## Introduction

This project contains code to access various Software as a Service (SaaS, or 'cloud') productivity tools, data sources and repositories. The code is written in the R Language,  commonly used for data sciences, and uses each SaaS tool's own Application Programming Interface (API) to read and write data. The idea is to be able to exchange data between different services, for migration, comparison, synchronisation, or any purpose you can dream up. 

### Handling complex data

Not only is the R language suited to large volume and complex datasets, but it offers a wide range of tools for handling and manipulating the data. APIs tend to return, or ingest, datasets  with a complex structure, often using the JSON format. The nested structure of these data objects fits well with the List data type that is one of the fundaments of R data storage and manipulation.

### Powerful language

R is not necessarily the most novice-friendly language – its syntax can seem odd, and strangely devoid of loops – there are also myriad different ways to achieve the same ends. However, as it offers powerful functional and array programming techniques it can be extremely powerful and relatively efficient once you get into the mindset. There are an extensive range of open and well-documented libraries available for all manner of data handling tasks, and a supportive community around them, so you are likely to become productive in new areas relatively quickly.

### Prototyping

The other reason why R lends itself to 'tinkering' and discovering how to manipulate new data sources and repos is because it is easy to use interactively. The R Studio development interface is freely available in package managers for most operating systems. It allows you to get working with existing code snippets quickly and flexibly, enhancing them on the fly, then browsing and interpreting data structures in multiple ways. This project starts off with only R source files and Rmd markdown code snippets, but hopefully could develop into Shiny apps and other ways to make interacting with data simpler for the person using it.

### screenshot

coming soon
## Usage

Install R and RStudio on your workstation using your system's repositories or package manager, then clone this repository.

_example instructions coming soon_

Each API will have private configuration details like endpoints, accounts, identities and secrets. You are encouraged to keep these safe, as you would passwords or private keys. You can keep as many environment snippets like these as you need, to access different systems and instances using different credentials, simply pasting in the ones you need interactively.

We may work towards the point where this project can help you select different ones as you read or write data, but for now we're keeping it simple

### Knitting as partial sourcing

The typical structure of projects here is a group of API specific functions in the first half of each Rmd, 
and a collection of development and testing and runtime code harnesses in the second half. 
You will see examples of this in 00 Template prototype, which you may copy to make any new .Rmd file.

Although knittr was designed to turn Rmd files into html output, we use Knitting as a way 
to execute only the first half of the Rmd, to set up the functions that might be used by another 
API's runtime code.For this reason the knit output goes to the .knitted folder which is 
considered as a runtime cache only, so excluded in .gitignore


## License

This project is copyrighted by it's author(s) but copylefted under 
**LGPL 2.1**, the GNU Lesser General Public License Version 2.1
so that it can be fairly shared - see [LICENSE](LICENSE).

## Development

### Useful libraries

If you are interested in developing an interface to a new service, 
do review the [CRAN Task View on Web Technologies and Services](https://cran.r-project.org/web/views/WebTechnologies.html), 
which introduces the large number of CRAN packages that 
might help you access or store data in common web-based services.

### Contributing

If you want to add to this project then check [CONTRIBUTING.md](CONTRIBUTING.md)

## ==================================
# old bits from OOD readme

_These are boilerplate, not specific to this project, just keeping them until deciding which to add into our README above_

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.


## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
